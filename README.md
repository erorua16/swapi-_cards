# SWAPI _CARDS


## TO START
1. Download project, in your terminal you can use the command `git clone git@gitlab.com:erorua16/swapi-_cards.git` or you can just download it as a zip
2. Run in terminal the following command to open a server: `php-S 0.0.0.0:8000`
3. Go to `http://0.0.0.0:8000/public/index.html`

## FUNCTIONALITIES
1. To get a card, click on the `get card` button. 
2. The timer will automatically set to 2 hours
3. To view all collected cards, click on `cards collection`. 

## FURTHER DEVELOPMENT
1. In your terminal run the command `npm init`
2. Install necessary packages using the command `npm i`
3. To compile scss to css run in your terminal the command `npm run comp`
