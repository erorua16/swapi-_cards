const getCard = document.getElementById('get-card')
const timer = document.getElementById('timer')
const card = document.getElementById('card')
const cardsCollection = document.getElementById('cardsCollection') 
var gotCard = []

//Get gotten cards information
if(localStorage.hasOwnProperty('cardsArray')){
    gotCard = JSON.parse(localStorage.getItem('cardsArray'))
}

////////////////////////////////////////////////////////////////FLIP CARD AND GET NEW CARD////////////////////////////////////////////////////////////////

//FLIP CARD
const flipCard = async() => {
    localStorage.removeItem("timer")
    timeSet(7200)
    notYetCardButton()
    randomizedCard().then( () => {
        card.classList.add('visible')
        setTimeout(() => {
            card.classList.remove('visible')
        }, 1000)
    })
    getCard.onclick = null;
    
}

//SET BUTTON TO GET CARD
const getCardButton = () => {
        //Change button information and style
        getCard.classList.remove('btn-outline-primary')
        getCard.classList.add('btn-primary')
        getCard.innerText = "Get Card!"

        //Add function add-card
        getCard.onclick = () => flipCard()
}

//SET BUTTON TO WAIT FOR CARD
const notYetCardButton = () => {
    //Change button information and style
    getCard.classList.add('btn-outline-primary')
    getCard.classList.remove('btn-primary')
    getCard.innerText = "Not Yet!"

}

////////////////////////////////////////////////////////////////TIMER////////////////////////////////////////////////////////////////
//SET TIMER
const timeSet = (timeleft) => {
        return cardTimer = setInterval(
            () => {
    
                //When timer runs out
                if(timeleft <= 0) {
                    
                getCardButton()
    
                clearInterval(cardTimer)
    
                }
    
            timer.innerText = formatTime(timeleft);
            timeleft --
            localStorage.setItem('timer', timeleft);
        }, 
        1000)
   
}

//CONVERT TO HOURS AND MINUTES
const formatTime = (timeleft) => {
        // Hours, minutes and seconds
        var hrs = ~~(timeleft / 3600);
        var mins = ~~((timeleft % 3600) / 60);
        var secs = ~~timeleft % 60;
    
        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";
        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }
        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
}


////////////////////////////////////////////////////////////////FETCH API////////////////////////////////////////////////////////////////
const baseURL = "https://swapi.dev/api/"

const categoriesArray = [
    "people",
    "planets",
    "species",
    "vehicles",
    "starships"
]

const randomizedCard = async () => {

     //Get randomized index
     const apiCategoryIndex = Math.floor(Math.random() * categoriesArray.length)
     const chosenCategory = categoriesArray[apiCategoryIndex]
     const vehicleNotCard = [
         1, 2, 3, 5, 9, 10, 11, 12, 13, 
         15, 17, 21, 22, 23, 27, 28, 29, 
         31, 32, 39
     ]
     const starshipNotCard = [
         1, 4, 6, 7, 8, 14, 16, 18, 19, 
         20, 24, 25, 26, 30, 33, 34, 35, 
         36 
     ]

     var randomNumber
     switch(chosenCategory){
         case "people": 
             randomNumber = Math.floor(Math.random() * (84 - 1) + 1)
             while (randomNumber == 17){
                randomNumber = Math.floor(Math.random() * (84 - 1) + 1)
             }
             break;
         case "planets":
             randomNumber = Math.floor(Math.random() * (61 + 1) + 1)
             break;
         case "species":
             randomNumber = Math.floor(Math.random() * (38 - 1) + 1)
             break;
         case "vehicles":
             randomNumber = Math.floor(Math.random() * (40 -1 ) + 1)
             while(vehicleNotCard.includes(randomNumber)){  
                randomNumber = Math.floor(Math.random() * (40 -1 ) + 1)
             }
             break;
         default:
             randomNumber = Math.floor(Math.random() * (37 - 1) + 1)
             while(starshipNotCard.includes(randomNumber)){  
                randomNumber = Math.floor(Math.random() * (37 - 1) + 1)
             }
             break;
     }
    
    var url = baseURL + chosenCategory + "/" + randomNumber
    
    if(gotCard.includes(url)){
             //Get randomized index
     const apiCategoryIndex = Math.floor(Math.random() * categoriesArray.length)
     const chosenCategory = categoriesArray[apiCategoryIndex]
     const vehicleNotCard = [
         1, 2, 3, 5, 9, 10, 11, 12, 13, 
         15, 17, 21, 22, 23, 27, 28, 29, 
         31, 32, 39
     ]
     const starshipNotCard = [
         1, 4, 6, 7, 8, 14, 16, 18, 19, 
         20, 24, 25, 26, 30, 33, 34, 35, 
         36 
     ]

     var randomNumber
     switch(chosenCategory){
         case "people": 
             randomNumber = Math.floor(Math.random() * (84 - 1) + 1)
             while (randomNumber == 17){
                randomNumber = Math.floor(Math.random() * (84 - 1) + 1)
             }
             break;
         case "planets":
             randomNumber = Math.floor(Math.random() * (61 + 1) + 1)
             break;
         case "species":
             randomNumber = Math.floor(Math.random() * (38 - 1) + 1)
             break;
         case "vehicles":
             randomNumber = Math.floor(Math.random() * (40 -1 ) + 1)
             while(vehicleNotCard.includes(randomNumber)){  
                randomNumber = Math.floor(Math.random() * (40 -1 ) + 1)
             }
             break;
         default:
             randomNumber = Math.floor(Math.random() * (37 - 1) + 1)
             while(starshipNotCard.includes(randomNumber)){  
                randomNumber = Math.floor(Math.random() * (37 - 1) + 1)
             }
             break;
     }
    
    url = baseURL + chosenCategory + "/" + randomNumber

    }else{
        await renderStarwars(url, 'card_name')
    }

}

const renderStarwars = async (url, docId) => {
    try{
        await fetch(url)
        .then(data => data.json())
        .then(data => {
            if (window.location.pathname == '/public/index.html') {
                document.getElementById(docId).textContent = data.name
                gotCard.push(url)
                localStorage.setItem('cardsArray', JSON.stringify(gotCard))
            }
            if (window.location.pathname == '/public/cardsCollection.html') {
                document.getElementById(docId).innerHTML += 

            `
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card shadow  py-0 rounded-lg ">
                    <div class="card-back card-face visible card-body py-3">
                        <div class="row align-items-center">
                            <div class="col">
                                <div class="my-3 text-center text-uppercase">
                                    <h4 class="text-truncate">${data.name}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            `
            }
        })
        }catch(error){
            console.error(error)
        }
}

//TO START TIMER
//if on timer page, launch timer
if (window.location.pathname == '/public/index.html') {
    if(localStorage.hasOwnProperty("timer")){
        if(localStorage.getItem('timer') > 0){
            timeSet(localStorage.getItem('timer'))
        }else {
            timer.innerText = formatTime(0);
            getCardButton()
        }
    }else {
        timeSet(7200)
    }
}
const getCardsCollection = async () => {
    cardsCollection.textContent = ""
    await gotCard.forEach( (e) => {
        renderStarwars(e, 'cardsCollection')
    })
}

//CARDS COLLECTION
if(gotCard.length > 0){
    getCardsCollection()
}else{
    cardsCollection.textContent = "No card yet! Get collecting!"
}
document.getElementById("numberOfCards").textContent = gotCard.length